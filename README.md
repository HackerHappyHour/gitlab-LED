# Gitlab LED

A tessel-powered library for controlling a strip of Analog RGB LED's based
on events taking place on a gitlab instance.

### Color Codes

| Color | Blink Rate | Event |
| ----- | ---------- | ----- |
| Red | 0 | Issue Open |
| Red | 75 | Build Failed |
| Green | 75 | Build Completed |
| Green | 0 | Issue Closed |
| Purple | 0 | Merge Request Accepted |
| 7 step RGB | 90 | New User |
| 3 step RGB | 60 | New Project |
